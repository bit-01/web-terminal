
// list commands
var list = (dest) => {
    dest.append("<dl class='commands'><div><dt>list</dt><dd>Lists all possible commands with an explination for each command</dd></div><div><dt>connect IP</dt><dt>connect SERVER NAME</dt><dd>Connects to the server specified by the IP or SERVER NAME <br> examples:<br>&nbsp;&nbsp; connect 45.32.192.15 <br>&nbsp;&nbsp; connect www.google.com <br>&nbsp;&nbsp; connect name</dd></div><div><dt>engage</dt><dd>HACK THE WORLD </dd></div><div><dt>su USERNAME</dt><dd>Switch user account into USERNAME<br>example: &nbsp;&nbsp; su root</dd></div><div><dt>cd PATH</dt><dd>Change current working directory to PATH <br>example: &nbsp;&nbsp; cd /mnt/d/folder</dd></div></dl>");
}

// print the input line for history
var add_bash_line = (dest, bash_line) => {

    dest.append("<section class='terminal' style='display:block;position: relative;padding: 0'><label><span>"+bash_line.server.text()+"</span>@<span>"+bash_line.username.text()+"</span>:<span>"+bash_line.path.text()+"</span>$</label>&nbsp;&nbsp;<span>"+bash_line.command+"</span></section>")
}


var connect = (inp, dest,bash_line) => {
    if(inp.length < 2) {
                    dest.append("<p class='error'>ERROR: Missing destination address</p>")
                } else if (inp.length > 2) {
                    dest.append("<p class='error'>ERROR: Wrong number of parameters</p>");
                    } else {
                      dest.append("<p class='connect'>Connecting to "+inp[1]+"......</p><progress min='0' max='100' value='0' class='prgs'></progress><span class='prg_p'>0 %</span>");
                    prgs = $('.prgs')
                    prgs.bar = $(prgs[prgs.length - 1])
                    prgs.num = $($('.prg_p')[prgs.length - 1])
                    setInterval(() => {
                        if(prgs.bar.val() < 100) {
                            prgs.bar.val(prgs.bar.val()+15)
                            prgs.num.text(prgs.bar.val()+" %")
                            if (prgs.bar.val() == 100) {
                                dest.append("<p class='success'>Connection successfull!!</p>")
                bash_line.server.text(inp[1])
                            }
                        }
                    }, 1000, prgs);
                
                    }
}



$(window).on('load', () => { 
    var canvas = $('#canvas')
    effectMatrix(canvas);
    function effectMatrix(neo) {
        var screen = window.screen; 
        var width = (neo[0].width = screen.width);
        var height = (neo[0].height = screen.height); 
        var letters = Array(256).join(1).split(""); 
        var designMatrix = function() { 

            neo[0].getContext("2d").fillStyle = "rgba(0,0,0,0.05)"; 
            neo[0].getContext("2d").fillRect(0, 0, width, height); 
            neo[0].getContext("2d").fillStyle = "#0F0"; 
            letters.map((position_y, index) => { 
                var text = String.fromCharCode(48 + Math.random() * 33); 
                var position_x = index * 10;
                neo[0].getContext("2d").fillText(text, position_x, position_y); 
                letters[index] = position_y > 758 + Math.random() * 1e4 ? 0 : position_y + 10; 
            }); 
         } 
         setInterval(designMatrix, 60); 
     }
});


var su = (dest, inp, bash_line) => {
    if(inp.length < 2) {
                    dest.append("<p class='error'>ERROR: Missing username</p>")
                } else if (inp.length > 2) {
                    dest.append("<p class='error'>ERROR: Wrong number of parameters</p>");
                    } else {
                    dest.append("<p>logging out.. <br>logged out</p>")
                    bash_line.username.text(inp[1])
                    
                    }
}

var cd = (dest, inp, bash_line) => {
    if(inp.length < 2) {
                    dest.append("<p class='error'>ERROR: Missing destination</p>")
                } else if (inp.length > 2) {
                    dest.append("<p class='error'>ERROR: Wrong number of parameters</p>");
                    } else {
                    bash_line.path.text(" "+inp[1]+" ")
                    }
}